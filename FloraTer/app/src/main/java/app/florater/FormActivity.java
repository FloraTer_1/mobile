package app.florater;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import app.florater.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FormActivity extends AppCompatActivity {
    private LocationManager locationManager;
    private LocationListener listener;
    private EditText Group;
    private EditText Place;
    private EditText Habitat;
    private EditText Date;
    private EditText GPS;
    private EditText Kwadrat_10x10;
    private EditText Kwadrat_5x5;
    private EditText Kwadrat_1x1;
    private EditText Kwadrat_01x01;
    private TextView Error;
    private ImageView GroupButton;
    private ImageView PlaceButton;
    private ImageView HabitatButton;
    private ImageView GPSButton;
    PlacesDB myDb;
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private android.widget.Button btnLocation;
    private TextView txtLocation;
    private android.widget.Button btnContinueLocation;
    private TextView txtContinueLocation;
    private StringBuilder stringBuilder;
    private boolean isContinue = false;
    private boolean isGPS = false;

    String wymaganePola = "Pole Nazwa stanowiska musi być wypełnione";
    Spannable centered = new SpannableString(wymaganePola);


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Button BackButton = (Button) findViewById(R.id.BackBtn);
        Button NextButton = (Button) findViewById(R.id.NextBtn);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        myDb = new PlacesDB(this);
        GroupButton = (ImageView) findViewById(R.id.iv_Group);
        PlaceButton = (ImageView) findViewById(R.id.iv_Place);
        HabitatButton = (ImageView) findViewById(R.id.iv_Habitat);
        GPSButton = (ImageView)findViewById(R.id.iv_GPS);
        Group = (EditText) findViewById(R.id.Group);
        Place = (EditText) findViewById(R.id.Place);
        Habitat = (EditText) findViewById(R.id.Habitat);
        Error = (TextView) findViewById(R.id.ErrorTextView);
        Date = (EditText) findViewById(R.id.Date);
        GPS = (EditText) findViewById(R.id.GPS);
        Kwadrat_10x10 = (EditText) findViewById(R.id.Kwadrat_10x10);
        Kwadrat_5x5 = (EditText) findViewById(R.id.Kwadrat_5x5);
        Kwadrat_1x1 = (EditText) findViewById(R.id.Kwadrat_1x1);
        Kwadrat_01x01 = (EditText) findViewById(R.id.Kwadrat_01x01);
        final String brakLokalizacji = "Lokalizacja wyłączona. Proszę włączyć lokalizację.";
        final ImageView DateButton = (ImageView) findViewById(R.id.iv_Date);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();


        centered.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),0,wymaganePola.length()-1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        Date.setFocusable(false);
        GPS.setFocusable(false);
        Kwadrat_10x10.setFocusable(false);
        Kwadrat_5x5.setFocusable(false);
        Kwadrat_1x1.setFocusable(false);
        Kwadrat_01x01.setFocusable(false);

        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                isGPS = isGPSEnable;
            }
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            GPS.setText(String.format(Locale.US, "%s  %s", wayLatitude, wayLongitude));
                            squares(wayLongitude,wayLatitude);
                        } else {
                            GPS.setText(String.format(Locale.US, "%s  %s", wayLatitude, wayLongitude));
                            squares(wayLongitude,wayLatitude);
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };



        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean val = validate();
                if (val == true) {
                    Error.setText("");
                    boolean isInserted = myDb.insertPlace(Place.getText().toString(), Group.getText().toString(), Habitat.getText().toString(), Date.getText().toString(), GPS.getText().toString());
                    if (isInserted = true) {
                        Toast.makeText(FormActivity.this, "Dodano stanowisko", Toast.LENGTH_SHORT).show();
                        editor.putString("Place", Place.getText().toString());
                        editor.commit();
                        startActivity(new Intent(FormActivity.this, SecondFormActivity.class));
                        FormActivity.this.finish();
                        Group.setText("");
                        Date.setText("");
                        Habitat.setText("");
                        Place.setText("");
                        GPS.setText("");
                        Kwadrat_10x10.setText("");
                        Kwadrat_5x5.setText("");
                        Kwadrat_1x1.setText("");
                        Kwadrat_01x01.setText("");
                    }
                }
            }
        });


        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FormActivity.this, MyformsActivity.class));
                FormActivity.this.finish();
            }
        });


        DateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date today = Calendar.getInstance().getTime();//getting date
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//formating according to my need
                String date = formatter.format(today);
                Date.setText(date);
            }
        });


        GroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Wprowadź numer grupy", 1);
            }
        });


        PlaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Wprowadź nazwę stanowiska", 2);
            }
        });


        HabitatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Wprowadź siedlisko", 3);
            }
        });



        GPSButton.setOnClickListener(v -> {
            if (!isGPS) {
                Toast.makeText(this, brakLokalizacji, Toast.LENGTH_SHORT).show();
                return;
            }
            isContinue = true;
            stringBuilder = new StringBuilder();
            getLocation();
        });





    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(FormActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(FormActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FormActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1000);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(FormActivity.this, location -> {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        GPS.setText(String.format(Locale.US, "%s %s", wayLatitude, wayLongitude));
                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(FormActivity.this, location -> {
                            if (location != null) {
                                wayLatitude = location.getLatitude();
                                wayLongitude = location.getLongitude();
                                GPS.setText(String.format(Locale.US, "%s %s", wayLatitude, wayLongitude));
                                squares(wayLongitude,wayLatitude);
                            } else {
                                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        });
                    }
                }
                break;
            }
        }
    }


    public boolean validate(){
        Cursor stanowisko = myDb.getAllPlaces();
        if (TextUtils.isEmpty(Place.getText())){
            //String wypelnijStanowisko = "Pole Nazwa stanowiska musi być wypełnione";
            Toast.makeText(getApplicationContext(),centered,Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(),wypelnijStanowisko,Toast.LENGTH_LONG).show();
            return false;
        }
        if(stanowisko.getCount()>0){
            while(stanowisko.moveToNext()){
                if(stanowisko.getString(0).equals(Place.getText().toString())){
                    String juzDodane = "Stanowisko o tej nazwie już istnieje";
                    Toast.makeText(getApplicationContext(),juzDodane,Toast.LENGTH_LONG).show();

                    return false;
                }
            }
        }

        return true;
    }


    private  void startVoiceInput(String data, int REQ){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, data);
        try {
            startActivityForResult(intent, REQ);
        } catch (ActivityNotFoundException a) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1001) {
                isGPS = true;
            }
        }
        switch (requestCode) {
            case 1: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Group.setText(result.get(0));
                }
                break;
            }
            case 2: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Place.setText(result.get(0));
                }
                break;
            }
            case 3: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Habitat.setText(result.get(0));
                }
                break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(FormActivity.this, MyformsActivity.class));
        FormActivity.this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem item = menu.findItem(R.id.Info);
        item.setVisible(true);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.Info:
                Intent intent = new Intent(this, InfoPlaces.class);
                startActivity(intent);
                return true;

            case R.id.Help:
                Intent intent1 = new Intent(this, InfoPlaces.class);
                startActivity(intent1);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    public String getChar(double val){
        if (val<100) return("A");
        if (val<200) return("B");
        if (val<300) return("C");
        if (val<400) return("D");
        if (val<500) return("E");
        if (val<600) return("F");
        if (val<700) return("G");
        else return("X");
    };

    public int get10km(double val){
        return  (int)((val % 100)/10);
    }
    public int get5km(double val){
        return  (int)((val % 100 % 10) / 5);
    }
    public int get1km(double val){
        return  (int)(val % 100 % 10);
    }
    public int get01km(double val,double fullval){
        return  (int)((fullval-val)*10);
    }
    public void squares(double lon,double lat){
        double ZERO_E = 19;
        double ZERO_N = 52;
        double ZERO_E_RADIANS = Math.toRadians(ZERO_E);
        double ZERO_N_RADIANS = Math.toRadians(ZERO_N);
        double EARTH_RADIUS = 6390;
        lon = lon - ZERO_E;
        double lat_radians = Math.toRadians(lat);
        double lon_radians = Math.toRadians(lon);
        double radius = Math.cos(ZERO_N_RADIANS) / Math.sin(ZERO_N_RADIANS) - Math.tan(lat_radians - ZERO_N_RADIANS);
        double x = EARTH_RADIUS * (radius * Math.sin(lon_radians * Math.sin(ZERO_N_RADIANS))) + 330;
        double y = EARTH_RADIUS * (radius * Math.cos(lon_radians*Math.sin(ZERO_N_RADIANS)) - Math.cos(ZERO_N_RADIANS)/Math.sin(ZERO_N_RADIANS))+350;
        int x_int = (int)x;
        int y_int = (int)y;

        Kwadrat_01x01.setText(getChar(x_int)+getChar(y_int)+String.valueOf(get10km(y_int))+String.valueOf(get10km(x_int))+String.valueOf(get1km(y_int))+String.valueOf(get1km(x_int))+String.valueOf(get01km(y_int,y))+String.valueOf(get01km(x_int,x)));
        Kwadrat_1x1.setText(getChar(x_int)+getChar(y_int)+String.valueOf(get10km(y_int))+String.valueOf(get10km(x_int))+String.valueOf(get1km(y_int))+String.valueOf(get1km(x_int)));
        Kwadrat_10x10.setText(getChar(x_int)+getChar(y_int)+String.valueOf(get10km(y_int))+String.valueOf(get10km(x_int)));
        Kwadrat_5x5.setText(getChar(x_int)+getChar(y_int)+String.valueOf(get10km(y_int))+String.valueOf(get10km(x_int))+String.valueOf(get5km(y_int))+String.valueOf(get5km(x_int)));
    }

}