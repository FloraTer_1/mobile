package app.florater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.florater.R;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        final LinearLayout Sebastian = (LinearLayout)findViewById(R.id.SebastianLinear);
        final LinearLayout Mikolaj = (LinearLayout)findViewById(R.id.MikolajLinear);
        final LinearLayout Artur = (LinearLayout)findViewById(R.id.ArturLinear);
        final LinearLayout Krzysztof = (LinearLayout)findViewById(R.id.KrzysztofLinear);

        final TextView KrzysztofInfo = (TextView) findViewById(R.id.tv_KrzysztofInfo);
        final TextView SebastianInfo = (TextView) findViewById(R.id.tv_SebastianInfo);
        final TextView MikolajInfo = (TextView) findViewById(R.id.tv_MikolajInfo);
        final TextView ArturInfo = (TextView) findViewById(R.id.tv_ArturInfo);


        Krzysztof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SebastianInfo.setText("");
                ArturInfo.setText("");
                MikolajInfo.setText("");
                KrzysztofInfo.setText("Python + Django");
                Krzysztof.setEnabled(false);
                Mikolaj.setEnabled(true);
                Sebastian.setEnabled(true);
                Artur.setEnabled(true);
            }
        });


        Sebastian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SebastianInfo.setText("Java");
                ArturInfo.setText("");
                MikolajInfo.setText("");
                KrzysztofInfo.setText("");
                Krzysztof.setEnabled(true);
                Mikolaj.setEnabled(true);
                Sebastian.setEnabled(false);
                Artur.setEnabled(true);
            }
        });


        Mikolaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SebastianInfo.setText("");
                ArturInfo.setText("");
                MikolajInfo.setText("Java");
                KrzysztofInfo.setText("");
                Krzysztof.setEnabled(true);
                Mikolaj.setEnabled(false);
                Sebastian.setEnabled(true);
                Artur.setEnabled(true);
            }
        });


        Artur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SebastianInfo.setText("");
                ArturInfo.setText("JavaScript + Vue.js");
                MikolajInfo.setText("");
                KrzysztofInfo.setText("");
                Krzysztof.setEnabled(true);
                Mikolaj.setEnabled(true);
                Sebastian.setEnabled(true);
                Artur.setEnabled(false);
            }
        });


        Button BackBtn = (Button)findViewById(R.id.BackBtn);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AboutUsActivity.this.finish();
            }
        });
    }
}