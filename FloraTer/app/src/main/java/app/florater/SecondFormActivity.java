package app.florater;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import app.florater.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;

public class SecondFormActivity extends AppCompatActivity {

    private ImageView LatinaNameButton;
    private ImageView PolishNameButton;
    private ImageView ExtraInfoButton;
    private ImageView PictureButton;
    private ImageView Picture;


    String wymaganePola = "Pole Nazwa Polska, Nazwa Łacińska lub Informacje dodatkowe musi zostać wypełnione";
    Spannable centered = new SpannableString(wymaganePola);






    private EditText LatinaName;
    private EditText PolishName;
    private EditText ExtraInfo;

    private TextView Error;

    PlacesDB myDb;
    Integer resources;
    byte[] byteArray = {1};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_form);
        myDb = new PlacesDB(this);

        centered.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),0,wymaganePola.length()-1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        Button BackButton = (Button) findViewById(R.id.BackBttn);
        Button AddButton = (Button) findViewById(R.id.AddBttn);
        Button SendButton = (Button) findViewById(R.id.SendBttn);



        Error = (TextView)findViewById(R.id.ErrorTextView);
        LatinaName = (EditText) findViewById(R.id.et_LatinName);
        PolishName = (EditText) findViewById(R.id.et_PolsihName);
        ExtraInfo = (EditText) findViewById(R.id.et_ExtraInfo);

        final EditText Herbarium = (EditText) findViewById(R.id.et_Herbarium);
        final EditText Resources = (EditText) findViewById(R.id.et_Resources);
        final Switch Switch = (android.widget.Switch)findViewById(R.id.switchHerbarium);
        final CheckBox CheckBox1 = (CheckBox)findViewById(R.id.checkBoxResources1);
        final CheckBox CheckBox2 = (CheckBox)findViewById(R.id.checkBoxResources2);
        final CheckBox CheckBox3 = (CheckBox)findViewById(R.id.checkBoxResources3);

        LatinaNameButton = (ImageView)findViewById(R.id.iv_LatinNameMic);
        PolishNameButton = (ImageView)findViewById(R.id.iv_PolsihNameMic);
        ExtraInfoButton=(ImageView)findViewById(R.id.iv_ExtraInfoMic);
        PictureButton = (ImageView)findViewById(R.id.iv_pictureAdd);
        Picture = (ImageView)findViewById(R.id.iv_Picture);

        final SharedPreferences prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        Herbarium.setFocusable(false);
        Resources.setFocusable(false);


        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), FormActivity.class);
                startActivity(intent);
                SecondFormActivity.this.finish();
            }
        });

        SendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), MyformsActivity.class);
                startActivity(intent);
                SecondFormActivity.this.finish();
            }
        });



        CheckBox1.setChecked(true);
        CheckBox2.setChecked(false);
        CheckBox3.setChecked(false);
        CheckBox1.setEnabled(false);
        CheckBox2.setEnabled(true);
        CheckBox3.setEnabled(true);
        Resources.setText("1");
        resources = 1;


        CheckBox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox2.setChecked(false);
                CheckBox3.setChecked(false);
                CheckBox1.setEnabled(false);
                CheckBox2.setEnabled(true);
                CheckBox3.setEnabled(true);
                Resources.setText("1");
                resources = 1;
            }
        });


        CheckBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox1.setChecked(false);
                CheckBox3.setChecked(false);
                Resources.setText("2");
                CheckBox1.setEnabled(true);
                CheckBox2.setEnabled(false);
                CheckBox3.setEnabled(true);
                resources = 2;
            }
        });


        CheckBox3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox2.setChecked(false);
                CheckBox1.setChecked(false);
                Resources.setText("3");
                CheckBox1.setEnabled(true);
                CheckBox2.setEnabled(true);
                CheckBox3.setEnabled(false);
                resources = 3;
            }
        });


        Switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    Herbarium.setText("Tak");

                }
                else {
                    Herbarium.setText("Nie");
                }
            }
        });


        LatinaNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Podaj łacińską nazwę",1);
            }
        });



        PolishNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Podaj polską nazwę",2);
            }
        });


        ExtraInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput("Wprowadź dodatkowe informacje",3);
            }
        });


        PictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,4);

            }
        });


        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean val = validate(prefs.getString("Place","brak"));
                if(val==true) {
                    //Error.setText("");
                    boolean isInserted = myDb.insertPlant(prefs.getString("Place", "Unknown"), LatinaName.getText().toString(), PolishName.getText().toString(), Herbarium.getText().toString(), resources, ExtraInfo.getText().toString(),byteArray);
                    if (isInserted = true) {
                        Toast.makeText(SecondFormActivity.this, "Dodano roślinę", Toast.LENGTH_SHORT).show();
                        LatinaName.setText("");
                        PolishName.setText("");
                        ExtraInfo.setText("");
                        Switch.setChecked(false);
                        CheckBox1.setChecked(true);
                        CheckBox2.setChecked(false);
                        CheckBox3.setChecked(false);
                        CheckBox1.setEnabled(false);
                        CheckBox2.setEnabled(true);
                        CheckBox3.setEnabled(true);
                        Resources.setText("1");
                        CheckBox2.setChecked(false);
                        CheckBox3.setChecked(false);
                        Picture.setImageBitmap(null);
                        byteArray = new byte[]{1};
                        resources = 1;

                    }
                }
            }

        });
    }




    public boolean validate(String Place){
        Cursor plants = myDb.getPlants(Place);
        if (TextUtils.isEmpty(LatinaName.getText()) && TextUtils.isEmpty(PolishName.getText()) && TextUtils.isEmpty(ExtraInfo.getText())){

            Toast.makeText(getApplicationContext(),centered,Toast.LENGTH_LONG).show();

            return false;
        }
        if(plants.getCount()>0){
            while(plants.moveToNext()){
                if(plants.getString(2).equals(LatinaName.getText().toString())&& !LatinaName.getText().toString().equals("")){
                    String juzDodane = "Wpisano już ten gatunek";
                    Toast.makeText(getApplicationContext(),juzDodane,Toast.LENGTH_LONG).show();

                    return false;
                }
                if(plants.getString(3).equals(PolishName.getText().toString()) && !PolishName.getText().toString().equals("")){
                    String juzDodane = "Wpisano już ten gatunek";
                    Toast.makeText(getApplicationContext(),juzDodane,Toast.LENGTH_LONG).show();

                    return false;
                }
            }
        }
        return true;

    }


    private  void startVoiceInput(String data, int REQ){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, data);
        try {
            startActivityForResult(intent, REQ);
        } catch (ActivityNotFoundException a) {

        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent (getApplicationContext(), FormActivity.class);
        startActivity(intent);
        SecondFormActivity.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    LatinaName.setText(result.get(0));
                }
                break;
            }
            case 2: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    PolishName.setText(result.get(0));
                }
                break;
            }
            case 3: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    ExtraInfo.setText(result.get(0));
                }
                break;
            }
            case 4:{
                if (resultCode == RESULT_OK){
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    Picture.setImageBitmap(rotateBitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    rotateBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                }
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem item = menu.findItem(R.id.Info);
        item.setVisible(true);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.Info:
                Intent intent = new Intent(this, InfoPlants.class);
                startActivity(intent);
                return true;

            case R.id.Help:
                Intent intent1 = new Intent(this, InfoPlaces.class);
                startActivity(intent1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}