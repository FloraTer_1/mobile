package app.florater;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import app.florater.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.Manifest.permission.READ_CONTACTS;



public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {
    PlacesDB myDb;
    Integer failure;
    String Token;
    String PlaceID;
    String PlantID;
    String text ="Nie udało się zalogować. Rośliny nie zostały wysłane. Spróbuj ponownie.";
    Spannable centered = new SpannableString(text);




    private static final int REQUEST_READ_CONTACTS = 0;


    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    private UserLoginTask mAuthTask = null;


    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        failure=0;
        centered.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),0,text.length()-1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        myDb = new PlacesDB(this);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        Button BackButton = (Button) findViewById(R.id.MenuButton);
        Button RegisterButton = (Button) findViewById(R.id.RegisterButton);
        Button LoginButton = (Button) findViewById(R.id.LoginButton);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        CheckBox remember = (CheckBox)findViewById(R.id.cb_remember);
        LoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        RegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        });
        BackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), MyformsActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        });
        mPasswordView.setText(prefs.getString("Password",""));
        mEmailView.setText(prefs.getString("Login",""));

        if(prefs.getInt("Remember",0)==1){
            remember.setChecked(true);
        }

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }



    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }


        mEmailView.setError(null);
        mPasswordView.setError(null);


        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError("Brak hasła");
            focusView = mPasswordView;
            cancel = true;
        }else if (!isPasswordValid(password)) {
            mPasswordView.setError("Hasło jest za krótkie");
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("Brak adresu e-mail");
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError("Błędny e-mail");
            focusView = mEmailView;
            cancel = true;
        }

        final TextView test = (TextView)findViewById(R.id.textView);
        final OkHttpClient client = new OkHttpClient();
        final MediaType MEDIA_TYPE = MediaType.parse("application/json");
        final JSONObject post = new JSONObject();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        try{
            post.put("email",mEmailView.getText());
            post.put("password",mPasswordView.getText());
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        if (cancel) {

            focusView.requestFocus();
        } else {
            String url = "http://florater.pythonanywhere.com/api/users/obtain-token/";
            RequestBody body = RequestBody.create(MEDIA_TYPE,post.toString());
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    String mMessage = e.getMessage().toString();
                    test.setText("Nie udało się połączyć z serwerem");
                    failure=1;
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if(response.isSuccessful()){
                        final String myResponse = response.body().string();
                        try {
                            JSONObject obj = new JSONObject(myResponse);

                            Token = obj.getString("token");

                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                CheckBox remember = (CheckBox)findViewById(R.id.cb_remember);
                                if(remember.isChecked()){
                                    editor.putString("Login",mEmailView.getText().toString());
                                    editor.putString("Password",mPasswordView.getText().toString());
                                    editor.putInt("Remember",1);
                                    editor.commit();
                                }
                                else{
                                    editor.remove("Login");
                                    editor.remove("Password");
                                    editor.putInt("Remember",0);
                                    editor.commit();
                                }

                                Cursor place = myDb.getAllPlaces();


                                StringBuffer buffer = new StringBuffer();
                                while(place.moveToNext()){
                                    String url = "http://florater.pythonanywhere.com/api/reports/main/";
                                    final String PlaceRemember = place.getString(0);

                                    final JSONObject Place = new JSONObject();
                                    try{
                                        if(place.getString(1).isEmpty()){}else{Place.put("group",place.getString(1));}
                                        if(place.getString(0).isEmpty()){}else{ Place.put("place",place.getString(0));}
                                        if(place.getString(2).isEmpty()){}else{Place.put("habitat",place.getString(2));}
                                        if(place.getString(3).isEmpty()){}else{Place.put("user_date",place.getString(3));}
                                        if(place.getString(4).isEmpty()){}else{Place.put("location",place.getString(4));}
                                        Place.put("owner",Token);
                                    }
                                    catch (JSONException e){
                                        e.printStackTrace();
                                    }
                                    RequestBody body = RequestBody.create(MEDIA_TYPE,Place.toString());
                                    Request request1 = new Request.Builder()
                                            .url(url)
                                            .post(body)
                                            .addHeader("Authorization", "JWT " + Token)
                                            .build();
                                    client.newCall(request1).enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            String mMessage = e.getMessage().toString();
                                            test.setText("Nie udało się połączyć z serwerem");
                                            failure=1;
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            if(response.isSuccessful()) {
                                                final String myResponse = response.body().string();
                                                try {
                                                    JSONObject obj = new JSONObject(myResponse);
                                                    PlaceID = obj.getString("id");
                                                } catch (Throwable t) {
                                                    t.printStackTrace();
                                                }

                                                final Cursor plant = myDb.getPlants(PlaceRemember);

                                                while(plant.moveToNext()){
                                                    String url2 = "http://florater.pythonanywhere.com/api/reports/plants/";
                                                    JSONObject Plant = new JSONObject();
                                                    byte[] photoArray = plant.getBlob(7);
                                                    try{
                                                        if(plant.getString(2).isEmpty()){}else{Plant.put("latin_name",plant.getString(2));}
                                                        if(plant.getString(3).isEmpty()){}else{Plant.put("polish_name",plant.getString(3));}
                                                        if (plant.getString(4).equals("Tak")) {
                                                            Plant.put("herbarium",true);
                                                        }
                                                        else
                                                        {
                                                            Plant.put("herbarium",false);
                                                        }
                                                        if(plant.getString(5).isEmpty()){}else{Plant.put("resources",plant.getInt(5));}
                                                        if(plant.getString(6).isEmpty()){}else{Plant.put("extra_info",plant.getString(6));}
                                                        Plant.put("report",PlaceID);

                                                    }
                                                    catch (JSONException e){
                                                        break;
                                                    }
                                                    RequestBody body = RequestBody.create(MEDIA_TYPE,Plant.toString());
                                                    Request request2 = new Request.Builder()
                                                            .url(url2)
                                                            .post(body)
                                                            .addHeader("Authorization", "JWT " + Token)
                                                            .build();



                                                    client.newCall(request2).enqueue(new Callback() {
                                                        @Override
                                                        public void onFailure(Call call, IOException e) {
                                                            String mMessage = e.getMessage().toString();
                                                            test.setText("Nie udało się połączyć z serwerem");
                                                            failure=1;
                                                        }

                                                        @Override
                                                        public void onResponse(Call call, Response response) throws IOException {
                                                            if(response.isSuccessful()) {
                                                                if (photoArray.length > 1) {
                                                                    final String myResponse = response.body().string();
                                                                    try {
                                                                        JSONObject obj = new JSONObject(myResponse);
                                                                        PlantID = obj.getString("id");
                                                                    } catch (Throwable t) {
                                                                        t.printStackTrace();
                                                                    }
                                                                    File photoFile = new File(getApplicationContext().getCacheDir(), PlantID);
                                                                    photoFile.createNewFile();
                                                                    FileOutputStream fos = new FileOutputStream(photoFile);
                                                                    fos.write(photoArray);
                                                                    fos.flush();
                                                                    fos.close();

                                                                    String url3 = "http://florater.pythonanywhere.com/api/reports/upload-photo/";
                                                                    RequestBody requestBody = new MultipartBody.Builder()
                                                                            .setType(MultipartBody.FORM)
                                                                            .addFormDataPart("reported_plant", PlantID)
                                                                            .addFormDataPart("photo", PlantID + ".png", RequestBody.create(MediaType.parse("image/png"), photoFile))
                                                                            .build();
                                                                    Request request3 = new Request.Builder()
                                                                            .url(url3)
                                                                            .put(requestBody)
                                                                            .addHeader("Authorization", "JWT " + Token)
                                                                            .build();
                                                                    client.newCall(request3).enqueue(new Callback() {
                                                                        @Override
                                                                        public void onFailure(Call call, IOException e) {
                                                                            String mMessage = e.getMessage().toString();
                                                                            test.setText("Nie udało się połączyć z serwerem");
                                                                            failure=1;
                                                                        }

                                                                        @Override
                                                                        public void onResponse(Call call, Response response) throws IOException {
                                                                            test.setText("udało sie");

                                                                        }
                                                                    });
                                                                }

                                                            }
                                                            else{
                                                            }

                                                        }
                                                    });
                                                }


                                            }
                                        }
                                    });

                                }

                            }
                        });

                    }
                    else{

                        test.setText("Nie udało się zalogować");
                        failure=1;
                    }
                }
            });
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    public boolean isEmailValid(String email) {

        return email.contains("@");
    }

    public boolean isPasswordValid(String password) {

        return password.length() > 4;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,

                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,


                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},


                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent (getApplicationContext(), MyformsActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }



    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {


            try {

                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }


            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {

                mPasswordView.setText("");


                if(failure==0) {
                    Cursor sta = myDb.getAllPlaces();
                    if (sta.getCount() > 0) {
                        while (sta.moveToNext()) {
                            myDb.DeletePlace(sta.getString(0));
                        }
                    }
                    Cursor ros = myDb.getAllPlants();
                    if (ros.getCount() > 0) {
                        while (ros.moveToNext()) {
                            myDb.DeletePlant(ros.getString(0));
                        }
                    }
                    Intent intent = new Intent (getApplicationContext(), MyformsActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                    Toast.makeText(getBaseContext(), "Wysłano pomyślnie", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),centered,Toast.LENGTH_LONG).show();
                }


            } else {
                mPasswordView.setError("Nieprawidłowe hasło.");
                mPasswordView.requestFocus();
            }
        }





        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}
