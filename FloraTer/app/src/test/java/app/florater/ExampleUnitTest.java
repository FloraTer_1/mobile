package app.florater;

import app.florater.FormActivity;
import app.florater.LoginActivity;
import app.florater.RegisterActivity;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleUnitTest {


    @Test
    public void RegisterEmailValidator1(){
        RegisterActivity tester = new RegisterActivity();
        assertTrue(tester.isEmailValid("name@mail.pl"));

    }
    @Test
    public void RegisterEmailValidator2(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isEmailValid("namemail.pl"));

    }
    @Test
    public void RegisterPasswordValidator1(){
        RegisterActivity tester = new RegisterActivity();
        assertTrue(tester.isPasswordValid("namemailpl"));

    }
    @Test
    public void RegisterPasswordValidator2(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isPasswordValid("abc"));

    }
    @Test
    public void RegisterNameValidator1(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isNameValid(""));

    }
    @Test
    public void RegisterNameValidator2(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isNameValid("abc@"));

    }
    @Test
    public void RegisterNameValidator3(){
        RegisterActivity tester = new RegisterActivity();
        assertTrue(tester.isNameValid("abc"));

    }
    @Test
    public void RegisterNameValidator4() {
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isNameValid("abc2"));
    }
    @Test
    public void RegisterSurnameValidator1(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isSurnameValid(""));

    }
    @Test
    public void RegisterSurnameValidator2(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isSurnameValid("abc@"));

    }
    @Test
    public void RegisterSurnameValidator3(){
        RegisterActivity tester = new RegisterActivity();
        assertTrue(tester.isSurnameValid("abc"));

    }
    @Test
    public void RegisterSurnameValidator4() {
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isSurnameValid("abc2"));
    }
    @Test
    public void RegisterIndexValidator1(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isIndexValid("12345"));

    }
    @Test
    public void RegisterIndexValidator2(){
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isIndexValid("1234567"));

    }
    @Test
    public void RegisterIndexValidator3(){
        RegisterActivity tester = new RegisterActivity();
        assertTrue(tester.isIndexValid("123456"));

    }
    @Test
    public void RegisterIndexValidator4() {
        RegisterActivity tester = new RegisterActivity();
        assertFalse(tester.isIndexValid("abcdef"));
    }

    @Test
    public void LoginEmailValidator1(){
        LoginActivity tester = new LoginActivity();
        assertTrue(tester.isEmailValid("name@mail.pl"));

    }
    @Test
    public void LoginEmailValidator2(){
        LoginActivity tester = new LoginActivity();
        assertFalse(tester.isEmailValid("namemail.pl"));

    }
    @Test
    public void LoginPasswordValidator1(){
        LoginActivity tester = new LoginActivity();
        assertTrue(tester.isPasswordValid("namemailpl"));

    }
    @Test
    public void LoginPasswordValidator2(){
        LoginActivity tester = new LoginActivity();
        assertFalse(tester.isPasswordValid("abc"));
    }
    @Test
    public void FormSquareValidator1(){
        FormActivity tester = new FormActivity();
        assertEquals(tester.getChar(120),"B");
    }
    @Test
    public void FormSquareValidator2(){
        FormActivity tester = new FormActivity();
        assertNotEquals(tester.getChar(120),"A");
    }
    @Test
    public void FormSquareValidator3(){
        FormActivity tester = new FormActivity();
        assertEquals(tester.get10km(159),5);
    }
    @Test
    public void FormSquareValidator4(){
        FormActivity tester = new FormActivity();
        assertNotEquals(tester.get10km(159),6);
    }
    @Test
    public void FormSquareValidator5(){
        FormActivity tester = new FormActivity();
        assertEquals(tester.get1km(159),9);
    }
    @Test
    public void FormSquareValidator6(){
        FormActivity tester = new FormActivity();
        assertNotEquals(tester.get1km(159),8);
    }
    @Test
    public void FormSquareValidator7(){
        FormActivity tester = new FormActivity();
        assertEquals(tester.get01km(120,124),40);
    }
    @Test
    public void FormSquareValidator8(){
        FormActivity tester = new FormActivity();
        assertNotEquals(tester.get01km(120,124),30);
    }
    @Test
    public void FormSquareValidator9(){
        FormActivity tester = new FormActivity();
        assertEquals(tester.get5km(159),1);
    }
    @Test
    public void FormSquareValidator10(){
        FormActivity tester = new FormActivity();
        assertNotEquals(tester.get5km(159),2);
    }


}