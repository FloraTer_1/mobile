package app.florater;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PlacesDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Places.db";
    public static final String TABLE_NAME1 = "Places";
    public static final String COL_10 = "Place";
    public static final String COL_11 = "GroupId";
    public static final String COL_12 = "Habitat";
    public static final String COL_13 = "Date";
    public static final String COL_14 = "GPS";
    public static final String TABLE_NAME2 = "Plants";
    public static final String COL_20 = "ID";
    public static final String COL_21 = "Place";
    public static final String COL_22 = "LatinName";
    public static final String COL_23 = "PolishName";
    public static final String COL_24 = "Herbarium";
    public static final String COL_25 = "Resources";
    public static final String COL_26 = "ExtraInfo";
    public static final String COL_27 = "Picture";


    public PlacesDB(Context context) {
        super(context, DATABASE_NAME,null,1);
        SQLiteDatabase db = this.getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME1 +" (Place TEXT PRIMARY KEY, GroupId TEXT,habitat TEXT,Date TEXT,GPS TEXT ) ");
        db.execSQL("create table " + TABLE_NAME2 +" (ID Integer Primary KEY AUTOINCREMENT ,Place TEXT, LatinName TEXT, PolishName TEXT, Herbarium TEXT, Resources INTEGER, ExtraInfo TEXT, Picture BLOB, FOREIGN KEY (Place) REFERENCES Places(Place)) ");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME1);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        onCreate(db);
    }


    public boolean insertPlace(String Place,String Group,String Habitat, String Date,String GPS){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_10,Place);
        contentValues.put(COL_11,Group);
        contentValues.put(COL_12,Habitat);
        contentValues.put(COL_13,Date);
        contentValues.put(COL_14,GPS);
        long result = db.insert(TABLE_NAME1,null,contentValues);
        if (result==-1){
            return false;
        }
        else{
            return  true;
        }
    }


    public boolean insertPlant(String Place, String LatinaName, String PolishName,
                               String Herbarium, Integer Resources, String ExtraInfo,
                               byte[] Picture){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_21,Place);
        contentValues.put(COL_22,LatinaName);
        contentValues.put(COL_23,PolishName);
        contentValues.put(COL_24,Herbarium);
        contentValues.put(COL_25,Resources);
        contentValues.put(COL_26,ExtraInfo);
        contentValues.put(COL_27,Picture);
        long result = db.insert(TABLE_NAME2,null,contentValues);
        if (result==-1){
            return false;
        }
        else{
            return  true;
        }
    }


    public Cursor getAllPlaces(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME1,null);
        return res;
    }


    public Cursor getPlants(String Place){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME2 + " WHERE Place LIKE " + "\"" + Place + "\" ",null);
        return res;
    }


    public Cursor getAllPlants(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME2,null);
        return res;
    }


    public Integer DeletePlant(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME2,"ID = ?",new String[]{id});
    }


    public Integer DeletePlace(String Place){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME1,"Place = ?",new String[]{Place});
    }


    public Integer getQuantityPlants(){
        int counter=0;
        String sql = " SELECT COUNT(*) FROM " + TABLE_NAME2;
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            counter=cursor.getInt(0);
        }
        cursor.close();
        return counter;
    }


    public Integer getQuanitityPlaces(){
        int counter=0;
        String sql = " SELECT COUNT(*) FROM " + TABLE_NAME1;
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            counter=cursor.getInt(0);
        }
        cursor.close();
        return counter;
    }
}

