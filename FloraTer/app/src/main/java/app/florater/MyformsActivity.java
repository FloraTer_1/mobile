package app.florater;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import app.florater.R;

import java.util.ArrayList;

public class MyformsActivity extends AppCompatActivity {

    Button LoginButton;
    PlacesDB myDb;
    ListView listView;

    ArrayList<String> listPlaces;
    ArrayList<String> listGroup;
    ArrayList<String> listHabitat;
    ArrayList<String> listGPS;
    ArrayList<String> listData;
    ArrayAdapter adapter;
    SwipeRefreshLayout refreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myforms);
        Button NewFormButton = (Button) findViewById(R.id.NewFormButton);
        LoginButton = (Button) findViewById(R.id.LoginButton);
        listView = (ListView)findViewById(R.id.List);
        myDb = new PlacesDB(this);

        SharedPreferences prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        if(prefs.getInt("WelcomeActivity",0)==0){
            Intent intent = new Intent(getApplicationContext(),Slide1Activity.class);
            startActivity(intent);
        }


        listPlaces = new ArrayList<>();
        listHabitat = new ArrayList<>();
        listGroup = new ArrayList<>();
        listGPS = new ArrayList<>();
        listData = new ArrayList<>();

        ViewData();

        NewFormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), FormActivity.class);
                startActivity(intent);
                MyformsActivity.this.finish();
            }
        });


        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                MyformsActivity.this.finish();
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String NazwaStanowiska = listView.getItemAtPosition(position).toString();
                editor.putString("Place", NazwaStanowiska);
                editor.commit();
                startActivity(new Intent(MyformsActivity.this, SecondFormActivity.class));
                MyformsActivity.this.finish();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem item = menu.findItem(R.id.aboutus);

        item.setVisible(true);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.aboutus:
                Intent intent1 = new Intent (getApplicationContext(), AboutUsActivity.class);
                startActivity(intent1);
                return true;

            case R.id.Help:
                Intent intent2 = new Intent (getApplicationContext(), HelpActivity.class);
                startActivity(intent2);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }






    private void ViewData(){
        Cursor cursor = myDb.getAllPlaces();
        listData.clear();
        listGPS.clear();
        listPlaces.clear();
        listGroup.clear();
        listHabitat.clear();
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                listPlaces.add(cursor.getString(0));
                listGroup.add(cursor.getString(1));
                listHabitat.add(cursor.getString(2));
                listData.add(cursor.getString(3));
                listGPS.add(cursor.getString(4));
            }
        }

        if(myDb.getQuanitityPlaces()<1){
            LoginButton.setVisibility(View.GONE);
        }
        else{
            LoginButton.setVisibility(View.VISIBLE);
        }

        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listPlaces);
        listView.setAdapter(adapter);
        if(cursor.getCount()<30){

        }
    }

    private void DeletePlaceFromList(String Place){
        Cursor plants = myDb.getPlants(Place);
        if (plants.getCount() > 0) {
            while (plants.moveToNext()) {
                myDb.DeletePlant(plants.getString(0));
            }
        }
        myDb.DeletePlace(Place);
        ViewData();
    }

}