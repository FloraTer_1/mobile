package app.florater;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Slide2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide2);
        SharedPreferences prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        Button next = (Button)findViewById(R.id.button3);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), Slide3Activity.class);
                startActivity(intent);
                Slide2Activity.this.finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent (getApplicationContext(), Slide1Activity.class);
        startActivity(intent);
        Slide2Activity.this.finish();
    }

}

