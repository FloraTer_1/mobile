package app.florater;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app.florater.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help);

        TextView Help1 = (TextView)findViewById(R.id.tv_Info1);
        TextView Help2 = (TextView)findViewById(R.id.tv_Info2);
        TextView Help3 = (TextView)findViewById(R.id.tv_Info3);
        TextView Help4 = (TextView)findViewById(R.id.tv_Info4);
        TextView Help5 = (TextView)findViewById(R.id.tv_Info5);
        TextView Help6 = (TextView)findViewById(R.id.tv_Info6);
        TextView Help7 = (TextView)findViewById(R.id.tv_Info7);


        String Help11 = "Najpierw należy dodać stanowisko. Aby to zrobić, kliknij przycisk \"Nowy spis\".";
        SpannableString ss1 = new SpannableString(Help11);
        ss1.setSpan(new StyleSpan(Typeface.BOLD),66,ss1.length()-1, 0);
        Help2.append(ss1);

        String Help22 = "Wypełnij dane dotyczące stanowiska, a następnie kliknij przycisk \"Dalej\".";
        SpannableString ss2 = new SpannableString(Help22);
        ss2.setSpan(new StyleSpan(Typeface.BOLD),65,ss2.length()-1, 0);
        Help3.append(ss2);

        String Help33 = "Po zakończeniu wykonywania spisu, kliknij przycisk \"Lista spisów\", a następnie \"Wyślij spisy\".";
        SpannableString ss3 = new SpannableString(Help33);
        ss3.setSpan(new StyleSpan(Typeface.BOLD),51,65, 0);
        ss3.setSpan(new StyleSpan(Typeface.BOLD),79,ss3.length()-1, 0);
        Help5.append(ss3);

        String Help44 = "Zaloguj się lub Stwórz konto (jeśli jeszcze go nie posiadasz). Kliknięcie przycisku \"Login\" przesyła Twoje dane na serwer. Aby nie wpisywać kolejny raz swoich danych zaznacz Zapamiętaj login i hasło.";
        SpannableString ss4 = new SpannableString(Help44);
        ss4.setSpan(new StyleSpan(Typeface.BOLD),84,91, 0);
        Help6.append(ss4);

        Help1.setText("Dane można wprowadzać z klawiatury i/lub głosowo. Poniżej w kilku prostych krokach znajduje się instrukcja obsługi.");
        Help4.setText("W widoku Roślina można dodawać gatunki roślin do utworzonego przed chwilą stanowiska.");
        Help7.setText("Dalsza praca nad danymi odbywa się na stronie internetowej.");


        Button BackBtn = (Button)findViewById(R.id.BackBtn);
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelpActivity.this.finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem item = menu.findItem(R.id.Help);
        item.setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){


            case R.id.Help:
                Intent intent2 = new Intent (getApplicationContext(), HelpActivity.class);
                startActivity(intent2);
                HelpActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}