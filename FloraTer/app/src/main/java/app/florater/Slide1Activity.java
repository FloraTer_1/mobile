package app.florater;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Slide1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide1);
        Button next = (Button)findViewById(R.id.button3);
        Button skip = (Button)findViewById(R.id.button2);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), Slide2Activity.class);
                startActivity(intent);
                Slide1Activity.this.finish();
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("WelcomeActivity", 1);
                editor.commit();
                Slide1Activity.this.finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        SharedPreferences prefs = getSharedPreferences("prefs",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("WelcomeActivity", 1);
        editor.commit();
        Slide1Activity.this.finish();
    }

}
